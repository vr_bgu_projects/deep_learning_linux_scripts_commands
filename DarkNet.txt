### DarkNet - Installed on machine with IP 16.34.225.167 ###
http://pjreddie.com/darknet/install/
#############################################
sudo apt-get install git unzip
cd /tmp
wget https://raw.githubusercontent.com/jayrambhia/Install-OpenCV/master/Ubuntu/2.4/opencv2_4_9.sh
sudo chmod +x opencv2_4_9.sh
./opencv2_4_9.sh


# reference: https://gist.github.com/titipata/f0ef48ad2f0ebc07bcb9
# check version
lspci | grep -i nvidia
uname -m && cat /etc/*release
gcc --version

sudo apt-get install linux-headers-$(uname -r)

# CUDA
cd /tmp
wget http://developer.download.nvidia.com/compute/cuda/7.5/Prod/local_installers/cuda_7.5.18_linux.run
sudo sh cuda_7.5.18_linux.run
sudo apt-get update
sudo apt-get install cuda
sudo reboot

vi ~/.bashrc
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib64:/usr/local/lib
export  CUDA_HOME=/usr/local/cuda
source ~/.bashrc

cd /usr/local

wget http://pjreddie.com/media/files/yolo.weights
git clone https://github.com/pjreddie/darknet.git
cd darknet


./darknet yolo test cfg/yolo.cfg /usr/local/darknet/yolo.weights data/dog.jpg 


