##Installed on machine 16.34.226.234####

git config --global url.https://github.com/.insteadOf git://github.com/
luarocks install torch
luarocks install nn
luarocks install image
luarocks install lua-cjson
luarocks install https://raw.githubusercontent.com/qassemoquab/stnbhwd/master/stnbhwd-scm-1.rockspec
luarocks install https://raw.githubusercontent.com/jcjohnson/torch-rnn/master/torch-rnn-scm-1.rockspec

cd /usr/local/torch

https://github.com/jcjohnson/densecap.git

sh scripts/download_pretrained_model.sh
This will download a zipped version of the model (about 1.1 GB) to 
data/models/densecap/densecap-pretrained-vgg16.t7.zip, 
unpack it to data/models/densecap/densecap-pretrained-vgg16.t7 (about 1.2 GB) 
and then delete the zipped version.


cd /usr/local/densecap
Running on new images [cpu ONLY]
th run_model.lua -input_image imgs/elephant.jpg  -gpu -1

This command will write results into the folder vis/data. We have provided a web-based visualizer to view these results; to use it, change to the vis directory and start a local HTTP server:

cd vis
python -m SimpleHTTPServer 8181
Then point your web browser to http://16.34.226.234:8181/view_results.html

If you have an entire directory of images on which you want to run the model, use the -input_dir flag instead:

th run_model.lua -input_dir /path/to/my/image/folder
This run the model on all files in the folder /path/to/my/image/folder/ whose filename does not start with ..

The web-based visualizer is the prefered way to view results, but if you don't want to use it then you can instead render an image with the detection boxes and captions "baked in"; add the flag -output_dir to specify a directory where output images should be written:

th run_model.lua -input_dir /path/to/my/image/folder -output_dir /path/to/output/folder/
The run_model.lua script has several other flags; you can find details here.


See the results
http://16.34.226.234:8181/view_results.html



