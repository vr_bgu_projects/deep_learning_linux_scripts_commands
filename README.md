# README #

**All the scripts for installing and using deep learning frameworks:
**
* tensorflow
* torch
* caffe
* overFeat
* dencsecap
* NeuralTalk
* Deep Detect


Also included general Linux commands & scripts.