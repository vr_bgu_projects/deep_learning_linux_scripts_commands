
Download model

wget http://www.deepdetect.com/models/age_model.tar.bz2

bzip2 -d age_model.tar.bz2
tar -xf age_model.tar


curl -X PUT "http://localhost:8080/services/age3" -d "{\"mllib\":\"caffe\",\"description\":\"age classification\",\"type\":\"supervised\",\"parameters\":{\"input\":{\"connector\":\"image\"},\"mllib\":{\"nclasses\":8}},\"model\":{\"repository\":\"/tmp/age_model\"}}"

curl -X POST "http://localhost:8080/predict" -d "{\"service\":\"age3\",\"parameters\":{\"output\":{\"best\":1}},\"data\":[\"/tmp/kid.jpg\"]}"




