sudo make pycaffe
sudo make distribute
# make dir for custom python modules, install caffe
mkdir ~/pycaffe
mv distribute/python/caffe ~/pycaffe

#################################################################
# set PYTHONPATH (this should go in your .bashrc or whatever
# export PYTHONPATH=${HOME}/pycaffe:$PYTHONPATH
echo 'export PYTHONPATH=/usr/local/caffe/python:$PYTHONPATH' >> ~/.bashrc 
#################################################################