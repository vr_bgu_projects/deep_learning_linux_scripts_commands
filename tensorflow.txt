sudo apt-get install python-pip python-dev

# Ubuntu/Linux 64-bit, CPU only, Python 2.7
export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-0.9.0rc0-cp27-none-linux_x86_64.whl


pip install --proxy http://web-proxy.isr.hp.com:8088 --upgrade https://storage.googleapis.com/tensorflow/linux/cpu/protobuf-3.0.0b2.post2-cp27-none-linux_x86_64.whl

sudo pip install --proxy http://web-proxy.isr.hp.com:8088 --upgrade $TF_BINARY_URL

sudo pip --proxy http://web-proxy.isr.hp.com:8088 uninstall six

sudo pip install --proxy http://web-proxy.isr.hp.com:8088 six --upgrade --target="/usr/lib/python2.7/dist-packages"

###CLASIFY IMAGE#####
cd usr/local/lib/python2.7/dist-packages/tensorflow/models/image/imagenet
python classify_image.py --image_file  /tmp/elephant.jpg